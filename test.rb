
require "minitest/autorun"
require_relative "parser"
require "pp"
class TestSieParser < Minitest::Test
	def test_from_string
		parser = SieParser.from_string "#hello world\n{\n\t#this \"is good\"\n}\n"
		sie = parser.parse
		
		assert_equal("world", sie.hello[0][0])
		assert_equal("is good", sie.hello[1].this)
	end

	def test_from_ugly_string
		parser = SieParser.from_string "#hello world { #this \"is good { hahaha }\" }"
		sie = parser.parse
		
		assert_equal("world", sie.hello[0][0])
		assert_equal("is good { hahaha }", sie.hello[1].this)
	end
		
	def test_from_file_with_utf8
		File.open("test1.sie") do |file|
			parser = SieParser.new [ :from_file, file ]
			sie = parser.parse
				
			assert_equal("0", sie.flagga)
			assert_equal([ "Incredibill", "10.0", "(8096b32f9a)" ], sie.program)
			assert_equal("PC8", sie.format)
			assert_equal("20141224", sie.gen)
			assert_equal("ovv.no (KCO)", sie.fnamn)

			assert_equal(4, sie.konto.length)
			assert_equal(4, sie.ktyp.length)
			assert_equal(4, sie.sru.length)

			assert_equal(2, sie.sru[0].length)
			assert_equal("1500", sie.sru[0][0])

			assert_equal("Fordran Klarna", sie.konto[0][1])

			assert_equal(2, sie.ver.length)
			assert_equal([ "1", "20140731", "Manuell" ], sie.ver[0])
		end	
	end
	
	def test_from_file_without_utf8
		File.open("test2.sie") do |file|
			parser = SieParser.new [ :from_file, file ]
			sie = parser.parse
			
			assert_equal("0", sie.flagga)
			assert_equal([ "Incredibill", "10.0", "(8096b32f9a)" ], sie.program)
			assert_equal("PC8", sie.format)
			assert_equal("20141224", sie.gen)
			assert_equal("ovv.no (KCO)", sie.fnamn)

			assert_equal(2, sie.konto.length)
			assert_equal(2, sie.ktyp.length)
			assert_equal(2, sie.sru.length)
			
			assert_equal("1921", sie.ver[1].trans[1][0])
			assert_equal("-4366.28", sie.ver[1].trans[0][2])
		end
	end
end