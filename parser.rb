class SieHash < Hash
	def method_missing(sym, *pars, &block)
		if self.has_key?(sym.to_s)
			return extract(self[sym.to_s])
		else
			super
		end
	end
	
	private
	
	def extract(val)
		if val.kind_of? Array
			if val.length > 1
				return val
			else
				return extract(val[0])
			end
		else
			return val
		end
	end
end

class SieParser
	attr_accessor :fields, :last, :record, :struct, :data

	def self.from_file file
		SieParser.new [ :from_file, file ]
	end
	
	def self.from_string string
		SieParser.new [ :from_string, string ]
	end

	def check_brackets_with_lookahead(input)
		string_parts = []
		
		input = input.gsub(/\"([^\"]*)\"/) do
			string_parts << Regexp.last_match.captures[0]
			"%!%%#{string_parts.length - 1}%%!%"
		end
			
		input = input.gsub(/\{\}/, "%%!!%%")
					 .gsub(/\{/, "\n{\n")
					 .gsub(/\}/, "\n}")
					 .gsub(/\n\n+/, "\n")
					 .gsub(/\{\s*\}/, "{}")
					 .gsub("%%!!%%", "{}")
		
		string_parts.each_index do |i|
			input = input.gsub("%!%%#{i}%%!%", "\"#{string_parts[i]}\"")
		end

		input
	end
		
	def initialize input
		@fields = SieHash.new
		@last = nil
		
		if input.kind_of?(Array)
			case input[0]
			when :from_file
				@data = input[1].read.scrub
			when :from_string
				@data = input[1].scrub
			end
		else
			@data = "#{input}"
		end
		
		@data = check_brackets_with_lookahead(@data)
	end	

	def parse_line line
		parts = line.split(" ")
		if parts.length == 0
			return
		end
		
		command = parts[0].gsub("#", "").downcase
		
		args = parts.drop(1)
		
		fargs = []
		current = "" 
		in_string = false
		args.each do |arg|
			unless in_string
				if arg == "{}"
					fargs << []
				elsif arg.start_with? "\""
					current = ""
					in_string = true
				else
					fargs << arg
				end
			end
			
			if in_string
				targ = arg.gsub("\"", "")
				current = "#{current} #{targ}"
				if arg.end_with?("\"")
					in_string = false
					current.strip!
					fargs << current
				end
			end
		end

		if @last.nil?
			unless @fields.has_key?(command)
				@fields[command] = []
			end

			@fields[command] << fargs
		else
			unless @struct.has_key?(command)
				@struct[command] = []
			end
			
			@struct[command] << fargs
		end
		
		return command
	end
	
	def parse(input=@data)
		lines = "#{input}".split("\n")
		lines.each do |line|
			line.strip!
			if line.length == 0 
				next
			end
			
			if line.start_with?("#")
				@record = parse_line line
			elsif line.start_with?("{")
				@last = @record
				@struct = SieHash.new
			elsif line.end_with?("}")
				@fields[@last] << @struct
			end
		end
		
		return @fields
	end	
end