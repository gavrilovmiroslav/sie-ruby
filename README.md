# Ruby SIE parser #

Easily parse .sie files into nestable, hashlike structures that defy the forces of nature and bend time and space.

## Why? ##

Because the other gems had trouble reading the files I needed read. Also, because this works with UTF8 data.

### How do I get set up? ###

Download the repo, include the ruby script and try it out. See the tests for code examples.

Basically, if you have a string, do:

    parser = SieParser.from_string "#hello world"
    result = parser.parse

This method is just shorthand for:

    SieParser.new [ :from_string, "#hello world" ]

If you have a file and want to read from it, you can do two things, again. Either go with:

    SieParser.new [ :from_file, "filename.sie" ]

or:

    SieParser.from_file "filename.sie"

The result of the operation will be a SieHash, which is basically an extended hash in which you can call the elements as methods, so that you can do this, in the example above:

    result.hello # => "world"

More complex expressions can have nested blocks in them, marked with { and }. For example:

    #hello world
    {
        #this and that
        #and this and that
    }

Creates the following structure:

    {
      "hello"=>
        [ 
          ["world"], 
          {
            "this"=>
            [
              ["and", "that"]
            ], 
            "and"=>
            [
              ["this", "and", "that"]
            ]
          }
        ]
    }

Which can be used in many ways:

    result.hello[1].this[1]  # => "that"
    result.hello[1].and[0]  # => "this"

Enjoy!